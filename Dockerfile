FROM python:3.7-alpine3.9

ENV SCREEN_WIDTH 1280
ENV SCREEN_HEIGHT 720
ENV SCREEN_DEPTH 16

COPY requirements.txt /tmp/requirements.txt

RUN apk update ;\
    apk add --no-cache chromium chromium-chromedriver udev xvfb firefox-esr ttf-freefont;\
    pip install --upgrade pip ;\
    pip install --no-cache-dir -r /tmp/requirements.txt ;\
    # Chrome requires docker to have cap_add: SYS_ADMIN if sandbox is on.
    # Disabling sandbox and gpu as default.
    sed -i "s/self._arguments\ =\ \[\]/self._arguments\ =\ \['\--headless'\,'\--no-sandbox'\,\'--disable-gpu'\,'\--disable-dev-shm-usage'\]/" $(python -c "import site; print(site.getsitepackages()[0])")/selenium/webdriver/chrome/options.py ;\
        # Install geckodriver
    wget -q https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz ;\
    tar -C /usr/bin/ -xf geckodriver-*.tar.gz ;\
    # List packages and python modules installed
    apk info -vv | sort ;\
    pip freeze ;\
    # Cleanup
    rm -rf /var/cache/apk/* /tmp/requirements.txt

